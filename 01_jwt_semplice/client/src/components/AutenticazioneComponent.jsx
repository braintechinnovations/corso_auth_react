import React from 'react'
import axios from 'axios'
import { useCookies } from 'react-cookie'

export function AutenticazioneComponent() {

    const [cookies, setCookies] = useCookies()

    const effettuaLogin = () => {
        axios.get("http://localhost:4000/login")
            .then((risultato) => {
                console.log(risultato.data)
                alert("Non riesco a formare il cookie ;(")
            })
            .catch((errore) => {
                console.log(errore)
            })

    }

    const effettuaLoginExposed = () => {
        axios.get("http://localhost:4000/loginexposed")
            .then((risultato) => {
                console.log(risultato.data)
                
                if(risultato.data.status && risultato.data.status == "SUCCESS")
                    setCookies("cookieAuth", risultato.data.token)
            })
            .catch((errore) => {
                console.log(errore)
            })
    }

    const verificaCookie = () => {
        console.log(cookies)
    }

    return (
        <div>
            <h1>Effettua operazioni</h1>
            <button onClick={effettuaLogin}>Login</button>
            <br />
            <button onClick={effettuaLoginExposed}>Login Exposed</button>
            <br />
            <button onClick={verificaCookie}>Verifica Cookie</button>
        </div>
    )

}

export default AutenticazioneComponent
