import logo from './logo.svg';
import './App.css';
import AutenticazioneComponent from './components/AutenticazioneComponent';

function App() {
  return (
    <div className="App">
      <AutenticazioneComponent />
    </div>
  );
}

export default App;
